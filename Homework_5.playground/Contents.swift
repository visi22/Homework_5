//: 1.Создать класс родитель и 2 класса наследника

class Animal {
    var height: Double
    var width: Double
    var weight: Double
    var name: String
    
    init(height: Double, width: Double, weight: Double, name: String) {
        self.height = height
        self.width = width
        self.weight = weight
        self.name = name
    }
    func eat() {
        print("I am eating")
    }
}

class Cat: Animal {
    override func eat() {
       print("I am eating cat's food")
    }
}

class Dog: Animal {
    override func eat() {
       print("I am eating dog's food")
    }
}


var firstdog = Dog(height: 5, width: 5, weight: 5, name: "Jake")
firstdog.name

//: 2.Написать программу, в которой создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов - *create*(выводит произведение свойств),*destroy*(отображает что дом уничтожен)

class House {
    var length: Double
    var width: Double
    var numberOfTenants: UInt
    
    init(length: Double, width: Double, numberOfTenants: UInt) {
        self.length = length
        self.width = width
        self.numberOfTenants = numberOfTenants
    }
    
    func create() {
        print("The area of the house is \(width * length )")
    }
    
    func destroy() {
        print("The house is destroyed")
    }
    
    func about() {
        print("This house has an area of \(width * length) m2, \(numberOfTenants) people live in it")
    }
    
}

var firstHouse = House(length: 10, width: 10, numberOfTenants: 5)
firstHouse.about()

//: 3.Создайте класс с методами, которые сортируют массив учеников

class ClassRoom {
    var students = ["Oleg", "Andrei", "Artem", "Angelina"]
    
    func sortUp() {
        let sortedArray = students.sorted(by: {$0.count < $1.count})
        print(sortedArray)
    }
    
    func sortDown() {
        let sortedArray = students.sorted(by: {$0.count > $1.count})
        print(sortedArray)
    }
}

var firstRoom = ClassRoom()
firstRoom.sortUp()
firstRoom.sortDown()


//: 4.Написать свою структуру и пояснить в комментариях - чем отличаются структуры от классов.

struct GamePlayer {
    var name: String = "Player"
    var victories: UInt = 0
    
    func about() {
        print("Player \(name) has \(victories)")
    }
}

var firstPlayer = GamePlayer(name: "Artem", victories: 5)

/* Класс - это ссылочный тип, экземпляры класса передаются по ссылке, а не копированием(структуры - Value type). У классов есть такие свойства, как наследование, полиморфизм. У структур есть встроенный инициализатор, который не требуется объявлять, а у классов есть только пустой встроенный. Так же у классов есть деинициализатор, который автоматически вызываестя при удалении экземпляра класса.*/

//: 5.Напишите простую программу, которая называет комбинации в покере

class MatchOfCards {
    let combination = ["Роял-флеш", "Стрит-флеш", "Каре", "Фул-Хаус", "Флеш", "Стрит", "Тройка или Сет", "Две пары", "Пара", "Высшая карта"]
    enum Suits: String {
        case heart = "червовый"
        case spade = "пиковый"
        case diamond = "бубновый"
        case club = "трефовый"
    }
    
    func aboutCombination(suit: Suits, combination: String) {
        print("У вас \(suit.rawValue) \(combination) ")
    }
    
}

var firstDone = MatchOfCards()
firstDone.aboutCombination(suit: .heart, combination: firstDone.combination[1])
